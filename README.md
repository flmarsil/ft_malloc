# ft_malloc

Coder la fonction malloc, free et realloc en C pour comprendre la gestion et l'allocation dynamique de mémoire.

### Construire et lancer le projet

```
git clone git@gitlab.com:flmarsil42/ft_malloc.git && cd ft_malloc && make
./start.sh file.c
```
